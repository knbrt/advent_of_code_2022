bans = []
open("./day_1/input_1.txt") do i
    ls = readlines(i)
    a = 0
    for l in ls
        if l == ""
            push!(bans, a)
            a = 0
        else
            a += parse(Int64, l)
        end
    end
end
ans = maximum(bans)
